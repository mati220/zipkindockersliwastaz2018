package pl.webnetix.zipkinclient.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

/*
 * @author      Mateusz Śliwa
*/
@Controller
public class ZipkinClientController {

    private static final Logger LOG = Logger.getLogger(ZipkinClientController.class.getName());

     @Autowired
     private RestTemplate restTemplate;

     @Bean
     public RestTemplate getRestTemplate() {
        return new RestTemplate();
     }

     @RequestMapping("/test")
     public ResponseEntity test(){
        LOG.info("Test from client");
         //double a = 2/0;
         return new ResponseEntity(restTemplate.getForEntity("http://localhost:8090/getMessage", String.class).getBody(), HttpStatus.OK);
     }

}

